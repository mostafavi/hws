from collections import defaultdict
from copy import deepcopy
from math import sqrt
from typing import List, Tuple
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation

import random

class Graph:
    class Vertex:
        pass

    vertex_type = Vertex

    def __init__(self):
        raise NotImplemented

    def vertices(self) -> List[Vertex]:
        raise NotImplemented

    def distance(self, node1: Vertex, node2: Vertex) -> float:
        raise NotImplemented

    def permutation_to_cycle(self, permutation: List[int]) -> List[Vertex]:
        '''
        maps permutations to cycles of the grah.
        '''
        nodes = self.vertices()
        assert(len(nodes) == len(permutation))
        return [nodes[index] for index in permutation]

    def cycle_length(self, cycle: List[Vertex]) -> float:
        shifted_cycle = cycle[1:] + [cycle[0]]
        return sum([self.distance(n1, n2) for n1, n2 in zip(cycle, shifted_cycle)])

    def permutation_length(self, permutation: List[int]) -> float:
        return self.cycle_length(self.permutation_to_cycle(permutation))

    def num_vertices(self) -> int:
        return len(self.vertices())

class AdjacencyMatrixGraph(Graph):
    def __init__(self, adj):
        self.adj = adj

    def vertices(self):
        return range(len(self.adj))

    def distance(self, node1, node2):
        return self.adj[node1][node2]

    def num_vertices(self):
        return len(self.adj)

class EuclideanGraph(Graph):
    def __init__(self, points):
        self.pts = points
    def vertices(self):
        return [point for point in self.pts]

    def distance(self, node1, node2):
        dx = node1[0] -  node2[0]
        dy = node1[1] -  node2[1]
        return sqrt(dx*dx + dy*dy)
    def num_vertices(self):
        return len(self.pts)

class EdgeListGraph(Graph):

    def __init__(self, edge_list):
        self.edges = edge_list
        self.cache_num_vertices = len(self.vertices)

    def vertices(self):
        nodes = [self.edges[0] for edge in self.edges] + [self.edges[1] for edge in self.edges]
        unique_nodes = list(set(nodes))
        return (node for node in nodes)

    def distance(self, node1, node2):
        for n1, n2, dist in self.edges:
            if (node1 == n1 and node2 == n2) or (node2 == n1 and node1 == n2):
                return dist
        return float('inf')

    def num_vertices(self):
        return self.cache_num_vertices

class GeneticAlgorithm:

    def __init__(self, population_size, mutation_probability, num_parents, max_iterations):
        '''
        num_parents: total number of selected parents in each generation
        '''
        self.population_size = population_size
        self.mutation_probability = mutation_probability
        self.num_parents = num_parents
        self.max_iterations = max_iterations

    def population_init(self):
        raise NotImplementedError

    def mutate(self, chromosome):
        raise NotImplementedError

    def cross(self, chromosome1, chromosome2):
        raise NotImplementedError

    def select_parents(self, population, num_pairs):
        total_fitness = sum([fitness for fitness, individual in population])
        rands = [random.random() for _ in range(2 * num_pairs)]
        cumulative_fitness = 0
        parents = []
        for individual_fitness_raw, individual in population:
            individual_fitness = individual_fitness_raw / total_fitness
            for rand in rands:
                if cumulative_fitness < rand <= (cumulative_fitness + individual_fitness):
                    parents.append(individual)
            cumulative_fitness += individual_fitness
        return zip(parents[::2], parents[1::2])

    def survival_selection(self, population, num_survivors):

        def sort_func(pop_item):
            fitness, individual = pop_item
            return fitness

        if num_survivors >= len(population):
            return population
        else:
            sorted_population = sorted(population, key=sort_func)
            return sorted_population[len(sorted_population) - num_survivors:]

    def run(self):
        current_population = self.population_init()
        average_fitness_values = []
        max_fitness_values = []
        sample_solutions = []
        self.init_run()

        for i in range(self.max_iterations):
            max_fitness_values.append(current_population[-1][0])
            average_fitness_values.append(self.average_fitness(current_population))
            self.process_population(current_population)
            parents = list(self.select_parents(current_population, self.num_parents))
            children = sum([self.cross(p1, p2) for (p1, p2) in parents], []) # flatten the list of children
            if random.random() < self.mutation_probability:
                children = [self.mutate(child) for child in children]
            children_with_fitness = [(self.fitness(child), child) for child in children]
            current_population += children_with_fitness
            current_population = self.survival_selection(current_population, self.population_size)
            sample_solutions.append(current_population[-1][1])
        return (current_population[-1], (average_fitness_values,
                                         max_fitness_values), sample_solutions, self.extra_results())
    def average_fitness(self, population):
        return sum([fitness for (fitness, _) in population]) / len(population)

    def init_run(self):
        pass

    def process_population(self, population):
        pass

    def extra_results(self):
        pass

    def fitness(self, individual):
        '''
        Must be positive. Objective is maximizing the fitness.
        '''
        raise NotImplementedError

class TSPGeneticAlgorithm(GeneticAlgorithm):

    def __init__(self, graph, population_size, mutation_probability, num_parents, max_iterations):
        super().__init__(population_size, mutation_probability, num_parents, max_iterations)
        self.graph = graph

    def population_init(self):
        res = []
        num_vertices = self.graph.num_vertices()
        for _ in range(self.population_size):
            p = list(range(num_vertices))
            random.shuffle(p)
            # res.append((p, self.fitness(p)))
            res.append((self.fitness(p), p))
        return res

    def fitness(self, chromosome):
        return 1 / (self.graph.permutation_length(chromosome))

    def init_run(self):
        self.average_edges = []

    def process_population(self, population):
        ps = population_size = len(population)
        cs = cycle_size = len(population[0][1])
        edge_counts = defaultdict(int)
        def ne(i):
            return (i+1) % cs
        for fitness, permutation in population:
            for i in range(len(permutation)):
                n1 = permutation[i]
                n2 = permutation[ne(i)]
                edge_counts[(min(n1, n2), max(n1, n2))] += 1
        self.average_edges.append(edge_counts)

    def extra_results(self):
        return self.average_edges

    def mutate(self, chromosome):
        copy = deepcopy(chromosome)
        if random.random() < self.mutation_probability:
            i, j = random.randrange(len(chromosome)), random.randrange(len(chromosome))
            copy[i], copy[j] = copy[j], copy[i]
        return copy

    def cross(self, chromosome1, chromosome2):
        chromosome_length  = len(chromosome1)
        index = random.randrange(chromosome_length)
        child1 = chromosome1[:index]
        child2 = chromosome2[:index]
        for i in range(chromosome_length):
            current_index = (index + i) % chromosome_length
            if chromosome1[current_index] not in child2:
                child2.append(chromosome1[current_index])
            if chromosome2[current_index] not in child1:
                child1.append(chromosome2[current_index])
        return [child1, child2]


def demo_tsp():
    num_points = 15
    x_range = (-5, 5)
    y_range = (-5, 5)

    plot_x_range = (-7, 7)
    plot_y_range = (-7, 7)

    # random.seed(0)
    random.seed(2734)

    def gen_point():
        return (random.uniform(*x_range), random.uniform(*y_range))

    random_points = [gen_point() for _ in range(num_points)]
    xs = [x for x,y in random_points]
    ys = [y for x,y in random_points]

    graph = EuclideanGraph(random_points)

    num_iterations = 1000
    population_size = 100

    tsp = TSPGeneticAlgorithm(graph, population_size, 1, 10, num_iterations)
    res, (average_fitness_values, max_fitness_values), sample_solutions, edge_counts = tsp.run()
    res_fitness, cycle = res

    # add the last edge back to the first node
    cycle = cycle + [cycle[0]]


    fig, ax = plt.subplots(2,2)
    ax0 = ax[0][0]
    ax1 = ax[0][1]
    ax2 = ax[1][0]
    ax3 = ax[1][1]

    # plt.subplot(221)
    # plt.scatter(xs, ys)
    # plt.plot([xs[i] for i in cycle], [ys[i] for i in cycle], 'black')

    # plt.xlim(plot_x_range)
    # plt.ylim(plot_y_range)

    # plt.subplot(222)
    # plt.plot(average_fitness_values, label="average fitness")
    # plt.plot(max_fitness_values, label="maximum fitness")
    # plt.legend()

    # ax = plt.subplot(223)
    # x_data= [0, 1]
    # y_data= [0, 1]

    ax0.scatter(xs, ys)
    ax0.plot([xs[i] for i in cycle], [ys[i] for i in cycle], 'black')

    ax0.set_xlim(plot_x_range)
    ax0.set_ylim(plot_y_range)

    ax1.plot(average_fitness_values, label="average fitness")
    ax1.plot(max_fitness_values, label="maximum fitness")
    max_scat = ax1.scatter([0], [0])
    ax1.legend()

    x_data= [0 for _ in range(len(random_points) + 1)]
    y_data= [0 for _ in range(len(random_points) + 1)]

    # x_data= [1]
    # y_data= [1]

    # scat = ax.scatter(x_data, y_data, color='yellow')
    plot = ax3.plot(x_data, y_data, color='red')[0]
    ax3.set_xlim(plot_x_range)
    ax3.set_ylim(plot_y_range)


    def init():
        ax.set_xlim(*x_range)
        ax.set_ylim(*y_range)

    def update(frame):
        frame = 10*frame
        current_sample = sample_solutions[frame]
        current_sample = current_sample + [current_sample[0]]
        # max_scat.set_offsets([[frame], [max_fitness_values[frame]]])
        max_scat.set_offsets([(frame, max_fitness_values[frame])])
        x_data = [random_points[index][0] for index in current_sample]
        y_data = [random_points[index][1] for index in current_sample]
        # print(x_data, y_data)
        plot.set_data(x_data, y_data)

    anim = FuncAnimation(fig, update, int(len(sample_solutions) / 10), interval=1)

    plt.show()



if __name__ == '__main__':
    demo_tsp()
