Run the tsp.ipynb file using jupyter notebook:
--> jupyter notebook tsp.ipynb
if you do not have jupyter you can install it from http://jupyter.org/.

report.html is a static version of the jupyter notebook file.
You can also run the tsp.py file which plots some of the statistics but it is not as comprehensive as the
notebook file.