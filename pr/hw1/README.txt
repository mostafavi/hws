Report and code are included in hw1.ipynb file. This is a jupyter notebook file that can be opened using the following command:
--> jupyter notebook hw1.ipynb

If you do not have jupyter installed, you can download it from http://jupyter.org/.
If you can not install jupyter, a prerendered version of the html document is included in hw1.htm file.
The following python libraries are used:
numpy: for some utility function
matplotlib: for plotting
scipy: for linear programming optimizer
bqplot: for interactive plots in ipython notebook
all of these libraries can be installed using pip (pip install numpy scipy matplotlib bqplot).

hw1.py is NOT the complete code and is an interactive toy subset. running it requires pygame (pip install pygame).