# todo: points with the same x coordinate mess up the convex hull algorithm

from itertools import combinations
from math import sqrt
from matplotlib import pyplot as plt
from numpy import argmin
from scipy.optimize import linprog
from typing import List, Tuple, Callable

import numpy as np
import random
import sys

Vector = List[float]
NormFunc = Callable[[Vector], float]
Point = Tuple[float, float]

EPSILON = sys.float_info.epsilon
# EPSILON = 0.001


class Line:
    @staticmethod
    def from_slope(slope: float, y0: float):
        return Line(slope, y0)

    @staticmethod
    def from_slope_and_point(slope: float, point: Tuple[float, float]):
        y0 = point[1] - point[0] * slope
        return Line.from_slope(slope, y0)

    @staticmethod
    def from_points(p0: Point, p1: Point):
        y_diff = p1[1] - p0[1]
        x_diff = p1[0] - p0[0]
        if abs(x_diff) > EPSILON:
            slope = y_diff / x_diff
        else:
            print("bad things")
            slope = float("inf")
        res = Line(slope, p0[1] - (slope * p0[0]))
        res.x0 = p0[0]
        return res

    def __init__(self, slope: float, y0: float):
        self.slope = slope
        self.y0 = y0

    def __call__(self, x: float) -> float:
        return self.y0 + x * self.slope

RegressionResult = Tuple[float, Line]

def compute_norm_inf(v: Vector) -> float:
    return max([abs(vi) for vi in v])

def compute_norm_2(v: Vector) -> float: # (squared)
    return sum([vi*vi for vi in v])

def compute_norm_1(v: Vector) -> float:
    return sum([abs(vi) for vi in v])

def compute_norm_0(v: Vector) -> float:
    return sum([1 for vi in v if abs(vi) > EPSILON])

def get_error(norm_fn: NormFunc, line: Line, xs: Vector, ys: Vector) -> float:
    y_primes = [line(x) for x in xs]
    error_vector = [y_prime - y for (y_prime, y) in zip(y_primes, ys)]
    return norm_fn(error_vector)

def get_mse(line, xs, ys):
    return get_error(compute_norm_2, line, xs, ys) / len(xs)

def norm_1_regression_lineprog(xs: Vector, ys: Vector) -> RegressionResult:
    pts : List[Point] = zip(xs, ys)
    number_of_linprog_variables = len(xs) + 2 # one for each point and two for w and b

    c = [1 for _ in range(len(xs))] + [0, 0]
    A_ub = []
    b_ub = []

    for i, (xi, yi) in enumerate(pts):
        prefix = [0 for _ in range(len(xs))]
        prefix[i] = -1
        A_ub.append(prefix + [xi, 1])
        b_ub.append(yi)

        A_ub.append(prefix + [-xi, -1])
        b_ub.append(-yi)

    a_bounds = (None, None)
    b_bounds = (None, None)
    c_bounds = [(0, None) for c in range(len(xs))]

    res = linprog(c, A_ub, b_ub, bounds=c_bounds + [a_bounds, b_bounds], method="interior-point")

    error = res.fun
    slope, y0 = res.x[-2], res.x[-1]

    return error, Line.from_slope(slope, y0)
def norm_inf_regression(xs: Vector, ys: Vector) -> RegressionResult:
    pts : List[Point] = zip(xs, ys)

    c = [0, 0, 1]
    A_ub = []
    b_ub = []

    for (xi, yi) in pts:
        A_ub.append([xi, 1, -1])
        b_ub.append(yi)

        A_ub.append([-xi, -1, -1])
        b_ub.append(-yi)

    a_bounds = (None, None)
    b_bounds = (None, None)
    c_bounds = (0, None)

    res = linprog(c, A_ub, b_ub, bounds=(a_bounds, b_bounds, c_bounds))

    error = res.fun
    slope, y0 = res.x[0], res.x[1]

    return error, Line.from_slope(slope, y0)

def norm_2_regression(xs: Vector, ys: Vector) -> RegressionResult:
    pts : List[Point] = zip(xs, ys)
    x_sum = sum(xs)
    x_squared_sum = sum([x*x for x in xs])
    y_sum = sum(ys)
    xy_sum = sum([x*y for (x,y) in pts])
    n = len(xs)

    slope = (x_sum*y_sum - n*xy_sum) / (x_sum*x_sum - n*x_squared_sum)
    y0 = (x_squared_sum * y_sum - xy_sum * x_sum) / (n*x_squared_sum - x_sum*x_sum)
    optimal_line = Line.from_slope(slope, y0)
    return get_error(compute_norm_2, optimal_line, xs, ys), optimal_line

def norm_1_regression(xs: Vector, ys: Vector) -> RegressionResult:
    pts : List[Point] = list(zip(xs, ys))
    lines = [Line.from_points(p0, p1) for (p0, p1) in combinations(pts, 2)]
    min_index = argmin([get_error(compute_norm_1, line, xs, ys) for line in lines])
    return get_error(compute_norm_1, lines[min_index], xs, ys), lines[min_index]

def norm_0_regression(xs: Vector, ys: Vector) -> RegressionResult:
    pts : List[Point] = zip(xs, ys)
    lines = [Line.from_points(p0, p1) for (p0, p1) in combinations(pts, 2)]
    min_index : int = argmin([get_error(compute_norm_0, line, xs, ys) for line in lines])
    return get_error(compute_norm_0, lines[min_index], xs, ys), lines[min_index]

def on_click(event):
    print(event.x, event.y)

def draw_axis(surface, window_width, window_height):
    half_width = int(window_width / 2)
    half_height = int(window_height / 2)
    pygame.draw.line(surface, (255, 255, 255), (0, half_height), (window_width, half_height))
    pygame.draw.line(surface, (255, 255, 255), (half_width, 0), (half_width, window_height))

def draw_point(surface, x, y, max_x, window_width, window_height, filled = False, color = (255, 0, 0), radius=5):
    window_scale = window_width / max_x / 2
    window_x_coord, window_y_coord = convert_plot_to_window_point((x, y), max_x, window_width, window_height)

    if filled:
        pygame.gfxdraw.filled_circle(surface, window_x_coord, window_y_coord, radius, color)

    pygame.gfxdraw.aacircle(surface, window_x_coord, window_y_coord, radius, color)

def draw_line(surface, line, max_x, window_width, window_height, color=(255, 0, 0)):
    '''
        assume the middle of the screen is (0,0)
    '''

    left_y = line(-max_x)
    right_y = line(max_x)

    window_scale = window_width / max_x / 2
    if line.slope == float("inf"):
        window_x = int(window_scale * line.x0 + window_width / 2)
        pygame.draw.line(surface, (255, 0, 0), (window_x, window_height), (window_x, 0))
    else:
        left_point_window_coord = convert_plot_to_window_point((-max_x, left_y), max_x, window_width, window_height)
        right_point_window_coord = convert_plot_to_window_point((max_x, right_y), max_x, window_width, window_height)
        pygame.draw.line(surface, color, left_point_window_coord, right_point_window_coord)

def convert_window_to_plot_point(window_point, max_x, window_width, window_height):
    window_scale = window_width / max_x / 2

    plot_x = (window_point[0] - window_width / 2) / window_scale
    plot_y = -(window_point[1] - window_height / 2) / window_scale

    return (plot_x, plot_y)

def convert_plot_to_window_point(plot_point, max_x, window_width, window_height):
    window_scale = window_width / max_x / 2

    window_x = plot_point[0] * window_scale + window_width / 2
    window_y = -plot_point[1] * window_scale + window_height / 2
    return (int(window_x), int(window_y))


def graham_scan_upper_hull(pts):
    '''
        assumed the x coordinate of all points are different
    '''

    assert(len(pts) >= 3)
    sorted_points = sorted(pts, key=lambda p: p[0])
    upper_hull = [sorted_points[0], sorted_points[1]]
    for p in sorted_points[2:]:
        while True:
            current_edge = Line.from_points(upper_hull[-1], p)
            if current_edge(upper_hull[-2][0]) < upper_hull[-2][1]:
                upper_hull.pop()
                if len(upper_hull) == 1:
                    upper_hull.append(p)
                    break
            else:
                upper_hull.append(p)
                break
    return upper_hull

def convex_hull(pts):
    upper_hull = graham_scan_upper_hull(pts)
    lower_hull = [(x, -y) for (x,y) in graham_scan_upper_hull([(x, -y) for (x,y) in pts])]
    lower_hull.reverse()
    return upper_hull + lower_hull[1:-1]


class PygameApplication:

    def __init__(self, window_width, window_height):
        pygame.init()
        self.window_width = window_width
        self.window_height = window_height
        self.surface = pygame.display.set_mode((window_width, window_height))


    def mainloop(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.display.quit()
                    return
                elif event.type == pygame.KEYDOWN:
                        self.on_key(event.key)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    mouse_x, mouse_y = pygame.mouse.get_pos()
                    self.on_mouse_down(mouse_x, mouse_y)

            self.surface.fill((0, 0, 0))
            self.draw()
            pygame.display.flip()

    def setup(self):
        pass

    def on_key(self, key):
        pass

    def on_mouse_down(self, mouse_x, mouse_y):
        pass

    def draw(self):
        pass

class RegressionPygameApplication(PygameApplication):

    def __init__(self, window_width, window_height, max_x):
        super().__init__(window_width, window_height)

        self.max_x = max_x
        self.font = pygame.font.SysFont("monaco", 30)
        self.norm_0_line_color = (255, 0, 0)
        self.norm_1_line_color = (255, 255, 0)
        self.norm_2_line_color = (0, 255, 0)
        self.norm_inf_line_color = (0, 0, 255)

        self.norm_0_text_surface = self.font.render("0", True, self.norm_0_line_color, (0, 0, 0))
        self.norm_1_text_surface = self.font.render("1", True, self.norm_1_line_color, (0, 0, 0))
        self.norm_2_text_surface = self.font.render("2", True, self.norm_2_line_color, (0, 0, 0))
        self.norm_inf_text_surface = self.font.render("inf", True, self.norm_inf_line_color, (0, 0, 0))

        self.xs = []
        self.ys = []
        self.lines = []

    def reset_lines(self):
        for line in self.lines:
            line.hide()

    def on_key(self, key):
        is_shift_pressed = pygame.key.get_pressed()[pygame.K_LSHIFT]
        if key == pygame.K_1:
            if is_shift_pressed:
                self.reset_lines()
                self.lines[1].show()
            else:
                self.lines[1].toggle()
        if key == pygame.K_2:
            if is_shift_pressed:
                self.reset_lines()
                self.lines[2].show()
            else:
                self.lines[2].toggle()
        if key == pygame.K_9:
            if is_shift_pressed:
                self.reset_lines()
                self.lines[3].show()
            else:
                self.lines[3].toggle()
        if key == pygame.K_0:
            if is_shift_pressed:
                self.reset_lines()
                self.lines[0].show()
            else:
                self.lines[0].toggle()

    def on_mouse_down(self, mouse_x, mouse_y):
        plot_x, plot_y = convert_window_to_plot_point((mouse_x, mouse_y), self.max_x, self.window_width, self.window_height)
        self.xs.append(plot_x)
        self.ys.append(plot_y)
        if len(self.xs) >= 2:
            _, norm_inf_line = norm_inf_regression(self.xs, self.ys)
            _, norm_2_line = norm_2_regression(self.xs, self.ys)
            #_, norm_1_line = norm_1_regression(self.xs, self.ys)
            _, norm_1_line = norm_1_regression_lineprog(self.xs, self.ys)
            _, norm_0_line = norm_0_regression(self.xs, self.ys)

            if self.lines == []:
                self.lines.append(WindowLine(norm_0_line,0, self.norm_0_line_color, True, self.font))
                self.lines.append(WindowLine(norm_1_line,1, self.norm_1_line_color, True, self.font))
                self.lines.append(WindowLine(norm_2_line,2, self.norm_2_line_color, True, self.font))
                self.lines.append(WindowLine(norm_inf_line,3, self.norm_inf_line_color, True, self.font))
            else:
                self.lines[0].line = norm_0_line
                self.lines[1].line = norm_1_line
                self.lines[2].line = norm_2_line
                self.lines[3].line = norm_inf_line


    def draw(self):
        draw_axis(self.surface, self.window_width, self.window_height)
        for line in self.lines:
            line.draw(self.surface, self.max_x, self.window_width, self.window_height)
        for x,y in zip(self.xs, self.ys):
            draw_point(self.surface, x, y, self.max_x, self.window_width, self.window_height, True, (0, 255, 0))

class ConverxHullApp(PygameApplication):

    def __init__(self, window_width, window_height, max_x):
        super().__init__(window_width, window_height)
        self.max_x = max_x
        self.pts = []
        self.hull = []

    def on_key(self, key):
        if key == pygame.K_ESCAPE:
            sys.exit()

    def on_mouse_down(self, mouse_x, mouse_y):
        plot_point = convert_window_to_plot_point((mouse_x, mouse_y), self.max_x, self.window_width, self.window_height)
        self.pts.append(plot_point)
        if len(self.pts) >= 3:
            self.hull = convex_hull(self.pts)

    def draw(self):
        self.draw_hull()
        for point in self.pts:
            draw_point(self.surface, point[0], point[1], self.max_x, self.window_width, self.window_height, filled=True, color=(255, 255, 255), radius=3)


    def draw_hull(self):
        if self.hull != []:
            shifted_hull = self.hull[1:] + [self.hull[0]]
            for p0, p1 in zip(self.hull, shifted_hull):
                window_point0 = convert_plot_to_window_point(p0, self.max_x, self.window_width, self.window_height)
                window_point1 = convert_plot_to_window_point(p1, self.max_x, self.window_width, self.window_height)
                pygame.draw.line(self.surface,(255,0,0), window_point0, window_point1)

def dot2(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1]

def find_extremes_in_direction(pts, direction):
    projections = [(point, dot2(point, direction)) for point in pts]

    return (np.argmin([x[1] for x in projections]), np.argmax([x[1] for x in projections]))

def antipodal_points(pts):

    class AntiIterator:

        def __init__(self, pts):
            self.pts = pts

        def __iter__(self):
            self.direction = (1, 0)
            self.current_antipodal_indices = find_extremes_in_direction(self.pts, self.direction)
            self.initial_antipodal_indices = self.current_antipodal_indices
            self.initial_min_index = self.current_antipodal_indices[0]
            self.initial_max_index = self.current_antipodal_indices[1]
            #print("initial indices: {}".format(self.initial_antipodal_indices))
            self.just_begun = True
            return self

        def __next__(self):
            if self.just_begun:
                self.just_begun = False
                return self.initial_antipodal_indices

            self.just_begun = False
            i_min, i_max = self.current_antipodal_indices

            np = len(self.pts)

            p_min = self.pts[i_min]
            p_min_next = self.pts[(i_min + 1) % np]

            p_max = self.pts[i_max]
            p_max_next = self.pts[(i_max + 1) % np]

            min_line = Line.from_points(p_min, p_min_next)
            max_line = Line.from_points(p_max, p_max_next)

            def n(i):
                return (i+1) % np

            current_slope = None
            if self.current_antipodal_indices[0] == self.initial_max_index:
                current_slope = Line.from_points(self.pts[i_max], self.pts[n(i_max)])
                self.current_antipodal_indices = (i_min, (i_max + 1) % np)
            elif self.current_antipodal_indices[1] == self.initial_min_index:
                current_slope = Line.from_points(self.pts[i_min], self.pts[n(i_min)])
                self.current_antipodal_indices = ((i_min + 1) % np, i_max)
            elif min_line.slope >= max_line.slope:
                current_slope = Line.from_points(self.pts[i_min], self.pts[n(i_min)])
                self.current_antipodal_indices = ((i_min + 1) % np, i_max)
            else:
                current_slope = Line.from_points(self.pts[i_max], self.pts[n(i_max)])
                self.current_antipodal_indices = (i_min, (i_max + 1) % np)
            if (self.current_antipodal_indices[0] == self.initial_antipodal_indices[1]
                and self.current_antipodal_indices[1] == self.initial_antipodal_indices[0]):
                raise StopIteration
            return self.current_antipodal_indices

    return AntiIterator(pts)

def antipodal_points_with_edges(pts):
    antipodal_pts = list(antipodal_points(pts))
    res = []
    index = 0
    def ne(i):
        return (i+1)%len(antipodal_pts)
    for i in range(len(antipodal_pts)):
        current_index = i
        next_index = ne(i)
        if next_index == 0:
            antipodal_pts[next_index] = antipodal_pts[next_index][1],antipodal_pts[next_index][0]
        if antipodal_pts[current_index][0] == antipodal_pts[next_index][0]:
            common = pts[antipodal_pts[current_index][0]]
            tangent_line = Line.from_points(pts[antipodal_pts[current_index][1]],
                                            pts[antipodal_pts[next_index][1]])
            res.append((common, pts[antipodal_pts[current_index][1]], tangent_line.slope))
            res.append((common, pts[antipodal_pts[next_index][1]], tangent_line.slope))
        else:
            common = pts[antipodal_pts[current_index][1]]
            tangent_line = Line.from_points(pts[antipodal_pts[current_index][0]],
                                            pts[antipodal_pts[next_index][0]])
            res.append((pts[antipodal_pts[current_index][0]], common, tangent_line.slope))
            res.append((pts[antipodal_pts[next_index][0]], common, tangent_line.slope))
    return res

def fitness(angle):
    direction = (np.cos(angle), np.sin(angle))
    direction = (direction[1], -direction[0])
    slope = 10

    try:
        slope = -direction[0] / direction[1]
    except:
        pass
    if abs(slope) > 1000:
        slope = 1000

    i_min, i_max = find_extremes_in_direction(ch, direction)
    p_min, p_max = ch[i_min], ch[i_max]

    return np.log(abs((-p_min[0] * slope + p_min[1]) - (-p_max[0] * slope + p_max[1]) ))

class WindowLine:

    def __init__(self, line, norm_index, color, visible, font):
        self.line = line
        self.color = color
        self.visible = visible
        self.norm_index = norm_index

        norm_name_map = ["0", "1", "2", "inf"]
        self.label_x_location_map = 20 * norm_index
        self.norm_name = norm_name_map[norm_index]
        self.label = font.render(self.norm_name, True, self.color, (0, 0, 0))


        self.lines = []

    def toggle(self):
        self.visible = not self.visible

    def hide(self):
        self.visible = False

    def show(self):
        self.visible = True

    def draw(self, screen, max_x, width, height):
        if self.visible:
            screen.blit(self.label, (self.label_x_location_map, 0))
            draw_line(screen, self.line, max_x, width, height, self.color)
            return True
        return False

def infinite_norm_antipodal(xs, ys):
    pts = list(zip(xs, ys))
    ch = convex_hull(pts)
    min_distance = float('inf')
    min_line = None

    for p1, p2, slope in antipodal_points_with_edges(ch):
        orthogonal_slope = -1/slope
        orthogonal_vector_size =sqrt(1 + 1/(slope*slope))
        orthogonal_vector = (1 / orthogonal_vector_size, -1/(slope * orthogonal_vector_size))
        i, j = find_extremes_in_direction(ch, orthogonal_vector)
        pi = ch[i]
        pj = ch[j]
        y0_i = pi[1] - pi[0] * slope
        y0_j = pj[1] - pj[0] * slope
        dist =abs(y0_i - y0_j)

        if dist < min_distance:
            min_distance = dist
            min_line = Line.from_slope(slope, (y0_i + y0_j)/2)

    return min_distance/2, min_line

def demo_antipodal_infinite_norm():
    points_x_range = (-5, 5)
    points_y_range = (-5, 5)
    window_x_lim = (-7, 7)
    window_y_lim = (-7, 7)

    num_points = 20

    def gen_point():
        x = random.uniform(*points_x_range)
        y = random.uniform(*points_y_range)

        return (x, y)

    random_points = [gen_point() for _ in range(num_points)]
    xs = [x for x,y in random_points]
    ys = [y for x,y in random_points]

    ch = convex_hull(random_points)
    ch_lines = ch + [ch[0]]

    plt.scatter(xs, ys)
    plt.plot([x for x,y in ch_lines], [y for x,y in ch_lines], 'b')

    error, line = infinite_norm_antipodal(xs, ys)
    ortho_slope = -1/line.slope
    ortho_direction = (1, ortho_slope)
    max_i, min_i = find_extremes_in_direction(ch, ortho_direction)

    max_point = ch[max_i]
    min_point = ch[min_i]

    min_line = Line.from_slope_and_point(line.slope, min_point)
    max_line = Line.from_slope_and_point(line.slope, max_point)
    plt.title("infinite norm using antipodal points")

    plt.plot([l for l in window_x_lim], [line(l) for l in window_x_lim], 'r')

    plt.plot([l for l in window_x_lim], [min_line(l) for l in window_x_lim], 'orange')
    plt.plot([l for l in window_x_lim], [max_line(l) for l in window_x_lim], 'orange')


    plt.xlim(window_x_lim)
    plt.ylim(window_y_lim)
    plt.ylabel("y")
    plt.xlabel("x")
    plt.show()


def demo_interactive():
    app = RegressionPygameApplication(500, 500, 5)
    app.mainloop()

if __name__ == '__main__':


    # app = RegressionPygameApplication(500, 500, 5)
    # app.mainloop()
    try:
        import pygame
        import pygame.gfxdraw
        demo_interactive()
    except ImportError:
        print("install pygame for interactive demo. (pip install pygame)")

    demo_antipodal_infinite_norm()



